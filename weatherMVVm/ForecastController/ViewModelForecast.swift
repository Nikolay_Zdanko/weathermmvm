//
//  ViewModelForecast.swift
//  weatherMVVm
//
//  Created by Николай on 17.09.21.
//

import Foundation
import Moya
import CoreLocation

class ViewModelForecast {
// MARK: - Variable
    var moyaProvider = MoyaProvider<WeatherService>()
    var temp = Bindable<Double?>(nil)
    var date = Bindable<Date?>(nil)
    var main = Bindable<String?>(nil)
    var nameCity = Bindable<String?>(nil)
    var list = Bindable<ModelForecast?>(nil)
    var networkingMonitor = NetworkMonitor.shared
    

    func getRequestForModeForecast() {
        LocationManager.shared.getLocation { [weak self] location in
            guard let self = self else { return }
            LocationManager.shared.resolveLocationName(with: location)
            self.moyaProvider.request(.forecast(lat: location.coordinate.latitude, lon: location.coordinate.longitude)) { result in
            switch result {
            case .success(let response):
                do {
                    let modelForecast = try JSONDecoder().decode(ModelForecast.self, from: response.data)
                    self.nameCity.value = modelForecast.city.name
                    self.list.value = modelForecast
                    for element in modelForecast.list {
//                        self.date.value = element.dt
                        self.temp.value = element.main.temp
                        for i in element.weather {
                            self.main.value = i.main
                        }
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
                }
            }
        }
    }
}
