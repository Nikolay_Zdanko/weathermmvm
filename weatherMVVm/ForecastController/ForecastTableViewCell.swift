//
//  ForecastTableViewCell.swift
//  weatherMVVm
//
//  Created by Николай on 17.09.21.
//

import UIKit

class ForecastTableViewCell: UITableViewCell {
//MARK: -Variable
    static let identifier = "ForecastTableViewCell"
    lazy var dateLabel: UILabel = {
        var dateLabel = UILabel()
        dateLabel.font = UIFont(name: "SFNS Display", size: 17)
        dateLabel.numberOfLines = 0
        return dateLabel
    }()
    lazy var mainLabel: UILabel = {
        var mainLabel = UILabel()
        mainLabel.font = UIFont(name: "SFNS Display", size: 20)
        return mainLabel
    }()
    lazy var tempLabel: UILabel = {
        var tempLabel = UILabel()
        tempLabel.font = UIFont(name: "SFNS Display", size: 55)
        return tempLabel
    }()
    lazy var imageWeather: UIImageView = {
        var imageWeather = UIImageView()
        return imageWeather
    }()
    // MARK: - Lifecycle function
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(dateLabel)
        contentView.addSubview(mainLabel)
        contentView.addSubview(imageWeather)
        contentView.addSubview(tempLabel)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        dateLabel.snp.makeConstraints { maker in
            maker.top.equalToSuperview().inset(20)
            maker.leading.equalToSuperview().inset(20)
        }
        mainLabel.snp.makeConstraints { maker in
            maker.top.equalTo(dateLabel).inset(55)
            maker.leading.equalToSuperview().inset(20)
        }
        tempLabel.snp.makeConstraints { maker in
            maker.bottom.top.equalToSuperview().inset(5)
            maker.trailing.equalToSuperview().inset(20)
        }
        imageWeather.snp.makeConstraints { maker in
            maker.trailing.equalTo(tempLabel).inset(50)
            maker.leading.equalToSuperview().inset(224)
            maker.bottom.top.equalToSuperview().inset(5)
        }
    }
// MARK: - func config for cell
    func config(with object: List) {
        tempLabel.text = "\(Int(object.main.temp))˚"
        dateLabel.text = object.dt.getFormattedDate(format: "dd MMMM\nHH:mm")
        for element in object.weather {
            mainLabel.text = element.main
            imageWeather.image = UIImage(named: element.main)
        }
    }
}
