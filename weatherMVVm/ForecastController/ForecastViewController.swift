//
//  ForecastViewController.swift
//  weatherMVVm
//
//  Created by Николай on 8.09.21.
//

import UIKit

class ForecastViewController: UIViewController {
    //MARK: -Variable
    var viewModelForecast: ViewModelForecast?
    lazy var tableView: UITableView = {
        var tableView = UITableView()
        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height ), style: .grouped)
        tableView.register(ForecastTableViewCell.self,
                           forCellReuseIdentifier: ForecastTableViewCell.identifier)
        //        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
// MARK: - Lifecycle function
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModelForecast = ViewModelForecast()
        self.title = "Forecast"
        self.view.backgroundColor = .white
        viewModelForecast?.getRequestForModeForecast()
        viewModelForecast?.list.bind({ _ in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.addSubview(tableView)
    }
    
    func returnSection(index: Int) -> Int{
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        let day = components.day
        guard let model = viewModelForecast?.list.value?.list else { return Int() }
        let result = model.filter({$0.dt.getFormattedDate(format: "dd") == "\((day ?? Int()) + index)"})
        return result.count
    }
    
    func setupCell(tableView: UITableView,indexPath: IndexPath, index: Int) -> UITableViewCell {
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        let day = components.day
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ForecastTableViewCell.identifier, for: indexPath) as? ForecastTableViewCell else { return UITableViewCell()}
        guard let model = viewModelForecast?.list.value?.list else { return UITableViewCell() }
        let result = model.filter({$0.dt.getFormattedDate(format: "dd") == "\((day ?? Int()) + index)"})
        cell.config(with: result[indexPath.row])
        return cell
    }
    
}
//MARK: -UITableViewDataSource
extension ForecastViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        for element in viewModelForecast?.list.value?.list ?? [] {
            let sectionTitle = section == 0 ? "Today" : element.dt.getFormattedDate(format: "EEEE", value: section)
            return sectionTitle
        }
        return ""
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
           return returnSection(index: 0)
        case 1:
           return returnSection(index: 1)
        case 2:
            return returnSection(index: 2)
        case 3:
            return returnSection(index: 3)
        case 4:
            return returnSection(index: 4)
        case 5:
            return returnSection(index: 5)
        case 6:
            return returnSection(index: 6)
        default:
            break
        }
        return (viewModelForecast?.list.value?.list.count ?? 0)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return setupCell(tableView: tableView, indexPath: indexPath, index: 0)
        case 1:
            return setupCell(tableView: tableView, indexPath: indexPath, index: 1)
        case 2:
            return setupCell(tableView: tableView, indexPath: indexPath, index: 2)
        case 3:
            return setupCell(tableView: tableView, indexPath: indexPath, index: 3)
        case 4:
            return setupCell(tableView: tableView, indexPath: indexPath, index: 4)
        case 5:
            return setupCell(tableView: tableView, indexPath: indexPath, index: 5)
        default:
            return UITableViewCell()
        }
    }
}
//MARK: -UITableViewDelegate
extension ForecastViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
