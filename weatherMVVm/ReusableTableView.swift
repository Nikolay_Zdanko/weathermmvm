//
//  ReusableTableView.swift
//  weatherMVVm
//
//  Created by Николай on 18.09.21.
//

import Foundation
import UIKit

class ReusableTableView<DataSource: Decodable, ReusableCell: UITableViewCell>: UITableView, UITableViewDelegate, UITableViewDataSource {
    var objects: [DataSource]
    var reusableIdentifier: String
    var completion: ((DataSource, ReusableCell)-> Void)
    
    init(objects: [DataSource], frame: CGRect, reusableIdentifier: String, completion: @escaping ((DataSource, ReusableCell)-> Void)) {
        self.completion = completion
        self.objects = objects
        self.reusableIdentifier = reusableIdentifier
        super.init(frame: frame, style: .grouped)
        self.delegate = self
        self.dataSource = self
        self.register(ReusableCell.self, forCellReuseIdentifier: reusableIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reusableIdentifier, for: indexPath) as? ReusableCell else { return UITableViewCell() }
        completion(objects[indexPath.row], cell)
        return cell
    }
    
    
}
extension ReusableTableView  {
    func reload(object: [DataSource]) {
        self.objects = object
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}
