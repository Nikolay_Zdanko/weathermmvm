//
//  PropertiesTableViewCell.swift
//  weatherMVVm
//
//  Created by Николай on 14.09.21.
//

import UIKit

class PropertiesTableViewCell: UITableViewCell {
// MARK: - Variable
    static let identifier = "PropertiesTableViewCell"
    lazy  var sunriseLabel: UILabel = {
        var sunriseLabel = UILabel()
        sunriseLabel.font = UIFont(name: "SFNS Display", size: 15)
        return sunriseLabel
    }()
    lazy  var sunriseValueLabel: UILabel = {
        var sunriseValueLabel = UILabel()
        sunriseValueLabel.font = UIFont(name: "SFNS Display", size: 16)
        return sunriseValueLabel
    }()
    lazy  var sunsetLabel: UILabel = {
        var sunsetLabel = UILabel()
        sunsetLabel.font = UIFont(name: "SFNS Display", size: 15)
        return sunsetLabel
    }()
    lazy  var sunsetValueLabel: UILabel = {
        var sunsetValueLabel = UILabel()
        sunsetValueLabel.font = UIFont(name: "SFNS Display", size: 16)
        return sunsetValueLabel
    }()
    
    lazy  var probabilityOfPrecipitationLabel: UILabel = {
        var probabilityOfPrecipitationLabel = UILabel()
        probabilityOfPrecipitationLabel.font = UIFont(name: "SFNS Display", size: 15)
        return probabilityOfPrecipitationLabel
    }()
    lazy  var probabilityOfPrecipitationValueLabel: UILabel = {
        var probabilityOfPrecipitationValueLabel = UILabel()
        probabilityOfPrecipitationValueLabel.font = UIFont(name: "SFNS Display", size: 16)
        return probabilityOfPrecipitationValueLabel
    }()
    lazy  var humidityLabel: UILabel = {
        var humidityLabel = UILabel()
        humidityLabel.font = UIFont(name: "SFNS Display", size: 15)
        return humidityLabel
    }()
    lazy  var humidityValueLabel: UILabel = {
        var humidityValueLabel = UILabel()
        humidityValueLabel.font = UIFont(name: "SFNS Display", size: 16)
        return humidityValueLabel
    }()
// MARK: - Lifecycle function
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(sunriseLabel)
        contentView.addSubview(sunriseValueLabel)
        contentView.addSubview(sunsetLabel)
        contentView.addSubview(sunsetValueLabel)
        contentView.addSubview(probabilityOfPrecipitationLabel)
        contentView.addSubview(probabilityOfPrecipitationValueLabel)
        contentView.addSubview(humidityLabel)
        contentView.addSubview(humidityValueLabel)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        sunriseLabel.snp.makeConstraints { maker in
            maker.top.equalToSuperview().inset(15)
            maker.leading.equalToSuperview().inset(50)
            maker.trailing.equalToSuperview().inset(240)
        }
        sunriseValueLabel.snp.makeConstraints { maker in
            maker.top.equalTo(sunriseLabel).inset(18)
            maker.leading.equalToSuperview().inset(50)
            maker.trailing.equalToSuperview().inset(270)
        }
        sunsetLabel.snp.makeConstraints { maker in
            maker.top.equalToSuperview().inset(15)
            maker.trailing.equalToSuperview().inset(60)
            maker.leading.equalToSuperview().inset(250)
        }
        sunsetValueLabel.snp.makeConstraints { maker in
            maker.top.equalTo(sunsetLabel).inset(18)
            maker.trailing.equalToSuperview().inset(100)
            maker.leading.equalToSuperview().inset(250)
        }
        
        probabilityOfPrecipitationLabel.snp.makeConstraints { maker in
            maker.top.equalTo(sunriseValueLabel).inset(35)
            maker.trailing.equalToSuperview().inset(140)
            maker.leading.equalToSuperview().inset(50)
        }
        probabilityOfPrecipitationValueLabel.snp.makeConstraints { maker in
            maker.top.equalTo(probabilityOfPrecipitationLabel).inset(18)
            maker.trailing.equalToSuperview().inset(210)
            maker.leading.equalToSuperview().inset(50)
        }
        
        humidityLabel.snp.makeConstraints { maker in
            maker.top.equalTo(sunsetValueLabel).inset(35)
            maker.trailing.equalToSuperview().inset(70)
            maker.leading.equalToSuperview().inset(250)
        }
        humidityValueLabel.snp.makeConstraints { maker in
            maker.top.equalTo(humidityLabel).inset(18)
            maker.trailing.equalToSuperview().inset(70)
            maker.leading.equalToSuperview().inset(250)
        }
    }
// MARK: - func config for cell
    func configure(with object: Current) {
        sunriseLabel.text = "Sunrise"
        sunsetLabel.text = "Sunset"
        sunriseValueLabel.text = object.sunrise?.getFormattedDate(format: "HH:mm")
        sunsetValueLabel.text = object.sunset?.getFormattedDate(format: "HH:mm")
        probabilityOfPrecipitationLabel.text = "Probability Of Precipitation"
        probabilityOfPrecipitationValueLabel.text = "100"
        humidityLabel.text = "Humidity"
        humidityValueLabel.text = "\(object.humidity ?? 0)"
    }
    
}
