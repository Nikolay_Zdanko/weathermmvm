//
//  OtherTableViewCell.swift
//  weatherMVVm
//
//  Created by Николай on 15.09.21.
//

import UIKit
import MapKit

class OtherTableViewCell: UITableViewCell {
// MARK: - Variable
    static let identifier = "OtherTableViewCell"
    var long: CLLocationDegrees?
    var lat: CLLocationDegrees?
    lazy var locationLabel: UILabel = {
        var locationLabel = UILabel()
        locationLabel.font = UIFont(name: "SFNS Display", size: 15)
        return locationLabel
    }()
    lazy var mapsButton: UIButton = {
        var mapsButton = UIButton(type: .system)
        mapsButton.setTitleColor(.black, for: .normal)
        mapsButton.setTitle("Open in maps", for: .normal)
        mapsButton.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return mapsButton
    }()
// MARK: - Lifecycle function
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(locationLabel)
        contentView.addSubview(mapsButton)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        locationLabel.snp.makeConstraints { maker in
            maker.top.equalToSuperview().inset(20)
            maker.leading.equalToSuperview().inset(20)
        }
        
        mapsButton.snp.makeConstraints { maker in
            maker.top.equalTo(locationLabel).inset(20)
            maker.leading.equalToSuperview().inset(20)
        }
    }
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        addSubview(SeparatorManager.shared.setSeparatorLineFor(style: 1, width: UIApplication.shared.windows[0].frame.width))
    }
//MARK: - action
    @objc func buttonTapped() {
        let regionDistance: CLLocationDistance = 1000
        let coordinates = CLLocationCoordinate2DMake(lat ?? CLLocationDegrees(), long ?? CLLocationDegrees())
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)]
        let placemark = MKPlacemark(coordinate: coordinates)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "I'm here"
        mapItem.openInMaps(launchOptions: options)
    }
// MARK: - func config for cell
    func configure(with object: Model) {
        locationLabel.text = "Weather - \(object.timezone ?? "")"
        getUnderLine()
    }
    func getUnderLine() {
        let attributes = [
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let attributeString = NSMutableAttributedString(string: "Open in maps", attributes: attributes)
        mapsButton.setAttributedTitle(attributeString, for: .normal)
    }
}
