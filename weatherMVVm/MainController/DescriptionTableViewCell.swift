//
//  DescriptionTableViewCell.swift
//  weatherMVVm
//
//  Created by Николай on 14.09.21.
//
import UIKit

class DescriptionTableViewCell: UITableViewCell {
// MARK: - Variable
    static let identifier = "DescriptionTableViewCell"
    lazy  var descriptionLabel: UILabel = {
        var descriptionLabel = UILabel()
        descriptionLabel.text = "1111"
        descriptionLabel.font = UIFont(name: "SFNS Display", size: 16)
        return descriptionLabel
    }()
// MARK: - Lifecycle function
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(descriptionLabel)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func didMoveToSuperview() {
            super.didMoveToSuperview()
            addSubview(SeparatorManager.shared.setSeparatorLineFor(style: 1, width: UIApplication.shared.windows[0].frame.width))
            addSubview(SeparatorManager.shared.setSeparatorLineFor(style: 4, width: UIApplication.shared.windows[0].frame.width))
        }
    override func layoutSubviews() {
        super.layoutSubviews()
        descriptionLabel.snp.makeConstraints { maker in
            maker.top.equalToSuperview().inset(15)
            maker.leading.equalToSuperview().inset(20)
        }
    }
}
