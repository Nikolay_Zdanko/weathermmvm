//
//  HeaderTableViewCell.swift
//  weatherMVVm
//
//  Created by Николай on 20.09.21.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {
// MARK: - Variable
    static let identifier = "HeaderTableViewCell"
    var viewModel: ViewModel?
    lazy var collectionView: UICollectionView = {
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: self.frame.size.width / 10,
                                 height: self.frame.size.width / 2.85)
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.register(CustomCollectionViewCell.self,
                                forCellWithReuseIdentifier: CustomCollectionViewCell.identifier)
        return collectionView
    }()
// MARK: - Lifecycle function
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(collectionView)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.snp.makeConstraints { maker in
            maker.leading.trailing.bottom.top.equalToSuperview().inset(0)
        }
    }
}
// MARK: - UICollectionViewDataSource
extension HeaderTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (viewModel?.hourlyArray.value?.count ?? 0) - 24
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomCollectionViewCell.identifier, for: indexPath) as? CustomCollectionViewCell else { return UICollectionViewCell() }
        guard let hourlyModel = viewModel?.hourlyArray.value?[indexPath.item] else { return UICollectionViewCell() }
        cell.viewModel = viewModel
        cell.configure(with: hourlyModel, indexPath: indexPath)
        return cell
    }
}
