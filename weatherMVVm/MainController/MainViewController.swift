//
//  ViewController.swift
//  weatherMVVm
//
//  Created by Николай on 8.09.21.
//

import UIKit
import SnapKit

class MainViewController: UIViewController {
    // MARK: - Variable
    lazy var timezoneLabel: UILabel = {
        var timezoneLabel = UILabel()
        timezoneLabel.font = UIFont(name: "SFNS Display", size: 20)
        return timezoneLabel
    }()
    lazy var dateLabel: UILabel = {
        var dateLabel = UILabel()
        dateLabel.font = UIFont(name: "SFNS Display", size: 18)
        return dateLabel
    }()
    lazy var mainWeather: UILabel = {
        var mainWeather = UILabel()
        mainWeather.font = UIFont(name: "SFNS Display", size: 22)
        return mainWeather
    }()
    lazy var currentTemp: UILabel = {
        var currentTemp = UILabel()
        currentTemp.font = UIFont(name: "SFNS Display", size: 80)
        return currentTemp
    }()
    lazy var imageMain: UIImageView = {
        var imageMain = UIImageView()
        imageMain.contentMode = .scaleAspectFit
        return imageMain
    }()
    lazy var maxLabelTemp: UILabel = {
        var currentTemp = UILabel()
        currentTemp.font = UIFont(name: "SFNS Display", size: 16)
        return currentTemp
    }()
    lazy var minLabelTemp: UILabel = {
        var currentTemp = UILabel()
        currentTemp.font = UIFont(name: "SFNS Display", size: 16)
        return currentTemp
    }()
    lazy var myView: UIView = {
        var myView = UIView()
        myView.backgroundColor = .clear
        return myView
    }()
    lazy var tableView: UITableView = {
        var tableView = UITableView()
        tableView = UITableView(frame: CGRect.zero, style: .grouped)
        tableView.register(HeaderTableViewCell.self,
                           forCellReuseIdentifier: HeaderTableViewCell.identifier)
        tableView.register(CustomTableViewCell.self,
                           forCellReuseIdentifier: CustomTableViewCell.identifier)
        tableView.register(DescriptionTableViewCell.self,
                           forCellReuseIdentifier: DescriptionTableViewCell.identifier)
        tableView.register(PropertiesTableViewCell.self,
                           forCellReuseIdentifier: PropertiesTableViewCell.identifier)
        tableView.register(OtherTableViewCell.self,
                           forCellReuseIdentifier: OtherTableViewCell.identifier)
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    var viewModel: ViewModel?
    // MARK: - Lifecycle function
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        viewModel = ViewModel()
        self.title = "Today"
        viewModel?.getRequestModel()
        bindOutlet()
        viewModel?.getDate()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addUIForView()
        myView.snp.makeConstraints { maker in
            maker.top.equalToSuperview().inset(0)
            maker.leading.trailing.equalToSuperview().inset(0)
            maker.bottom.equalTo(tableView).inset(0)
        }
        timezoneLabel.snp.makeConstraints { maker in
            maker.top.equalTo(myView).inset(40)
            maker.centerX.equalToSuperview()
        }
        dateLabel.snp.makeConstraints { maker in
            maker.top.equalTo(timezoneLabel).inset(30)
            maker.centerX.equalToSuperview()
        }
        mainWeather.snp.makeConstraints { maker in
            maker.top.equalTo(dateLabel).inset(40)
            maker.centerX.equalToSuperview()
        }
        currentTemp.snp.makeConstraints { maker in
            maker.top.equalTo(mainWeather).inset(30)
            maker.centerX.equalToSuperview()
        }
        imageMain.snp.makeConstraints { maker in
            maker.trailing.equalToSuperview().inset(190)
            maker.leading.equalToSuperview().inset(70)
            maker.top.bottom.equalTo(currentTemp).inset(0)
        }
        maxLabelTemp.snp.makeConstraints { maker in
            maker.top.equalTo(currentTemp).inset(120)
            maker.centerX.equalToSuperview()
        }
        minLabelTemp.snp.makeConstraints { maker in
            maker.top.equalTo(maxLabelTemp).inset(15)
            maker.centerX.equalToSuperview()
        }
        tableView.snp.makeConstraints { maker in
            maker.top.equalTo(minLabelTemp).inset(30)
            maker.bottom.leading.trailing.equalToSuperview().inset(0)
        }
    }
    func addUIForView() {
        view.addSubview(myView)
        myView.addSubview(timezoneLabel)
        myView.addSubview(dateLabel)
        myView.addSubview(mainWeather)
        myView.addSubview(imageMain)
        myView.addSubview(currentTemp)
        myView.addSubview(maxLabelTemp)
        myView.addSubview(minLabelTemp)
        view.addSubview(tableView)
    }
    
    func bindOutlet() {
        self.viewModel?.timeZone.bind({ value in
            self.timezoneLabel.text = value
        })
        self.viewModel?.dateNow.bind({ value in
            self.dateLabel.text = value
        })
        self.viewModel?.mainWeather.bind({ value in
            self.mainWeather.text = value
            self.imageMain.image = UIImage(named: value ?? "")
            self.view.setupGradient(main: value ?? "")
        })
        self.viewModel?.currentTemp.bind({ value in
            self.currentTemp.text = " \(Int(value ?? Double()))˚"
        })
        self.viewModel?.maxTemp.bind({ value in
            self.maxLabelTemp.text = value
        })
        self.viewModel?.minTemp.bind({ value in
            self.minLabelTemp.text = value
        })
        self.viewModel?.dayliArray.bind({ _ in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
    }
    
    
}
// MARK: - UITableViewDataSource
extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (viewModel?.dayliArray.value?.count ?? 0) + 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel, let value = viewModel.dayliArray.value else { return UITableViewCell()}
        if indexPath.row < value.count {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CustomTableViewCell.identifier, for: indexPath) as? CustomTableViewCell else { return UITableViewCell() }
            cell.configure(with: value[indexPath.row])
            cell.backgroundColor = .clear
            return cell
        } else if indexPath.row < value.count + 1 {
            guard let cellDescription = tableView.dequeueReusableCell(withIdentifier: DescriptionTableViewCell.identifier, for: indexPath) as? DescriptionTableViewCell else { return UITableViewCell() }
            cellDescription.descriptionLabel.text = viewModel.descriptionError.value
            cellDescription.backgroundColor = .clear
            return cellDescription
        } else if indexPath.row < value.count + 2  {
            guard let cellProperties = tableView.dequeueReusableCell(withIdentifier: PropertiesTableViewCell.identifier, for: indexPath) as? PropertiesTableViewCell else { return UITableViewCell() }
            guard let model = viewModel.currentModel.value else { return UITableViewCell() }
            cellProperties.configure(with: model)
            cellProperties.backgroundColor = .clear
            return cellProperties
        } else if indexPath.row < value.count + 3 {
            guard let cellOther = tableView.dequeueReusableCell(withIdentifier: OtherTableViewCell.identifier, for: indexPath) as? OtherTableViewCell else { return UITableViewCell() }
            guard let model = viewModel.jsonModel.value else { return UITableViewCell() }
            cellOther.configure(with: model)
            cellOther.backgroundColor = .clear
            return cellOther
        }
        return UITableViewCell()
    }
}
// MARK: - UITableViewDelegate
extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as? HeaderTableViewCell else { return UIView() }
        cell.viewModel = viewModel
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < viewModel?.dayliArray.value?.count ?? 0 {
            return 37
        } else if indexPath.row < (viewModel?.dayliArray.value?.count ?? 0) + 1 {
            return 40
        } else if indexPath.row < (viewModel?.dayliArray.value?.count ?? 0) + 2 {
            return 100
        } else if indexPath.row < (viewModel?.dayliArray.value?.count ?? 0) + 3 {
            return 60
        }
        return CGFloat()
    }
    // MARK: - ScrollViewDelegate
    func scrollViewDidScroll (_ scrollView: UIScrollView) {
        //        scrollView.setupMaskCellForScrollView(scrollView: scrollView, tableView: tableView)
    }
}
