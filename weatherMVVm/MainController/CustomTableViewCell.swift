//
//  CustomTableViewCell.swift
//  weatherMVVm
//
//  Created by Николай on 13.09.21.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
// MARK: - Variable
    static let identifier = "CustomTableViewCell"
    lazy var dayOfWeekLabel: UILabel = {
        var dayOfWeekLabel = UILabel()
        dayOfWeekLabel.font = UIFont(name: "SFNS Display", size: 16)
        return dayOfWeekLabel
    }()
    lazy var imageWeather: UIImageView = {
        var imageWeather = UIImageView()
        return imageWeather
    }()
    lazy var dayTempLabel: UILabel = {
        var dayTempLabel = UILabel()
        dayTempLabel.font = UIFont(name: "SFNS Display", size: 17)
        return dayTempLabel
    }()
    lazy var nightTempLabel: UILabel = {
        var nightTempLabel = UILabel()
        nightTempLabel.font = UIFont(name: "SFNS Display", size: 17)
        return nightTempLabel
    }()
// MARK: - Lifecycle function
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(dayOfWeekLabel)
        contentView.addSubview(imageWeather)
        contentView.addSubview(dayTempLabel)
        contentView.addSubview(nightTempLabel)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        dayOfWeekLabel.snp.makeConstraints { maker in
            maker.centerY.equalToSuperview()
            maker.leading.equalToSuperview().inset(20)
        }
        imageWeather.snp.makeConstraints { maker in
            maker.trailing.leading.equalToSuperview().inset(175)
            maker.top.bottom.equalToSuperview().inset(2)
        }
        dayTempLabel.snp.makeConstraints { maker in
            maker.centerY.equalToSuperview()
            maker.leading.equalTo(imageWeather).inset(120)
        }
        nightTempLabel.snp.makeConstraints { maker in
            maker.centerY.equalToSuperview()
            maker.leading.equalTo(dayTempLabel).inset(40)
        }
    }
// MARK: - func config for cell
    func configure(with object: Daily) {
        dayOfWeekLabel.text = object.dt?.addDailyDate(date: object.dt ?? 0, dateFormat: "EEEE")
        for element in object.weather {
            imageWeather.image = UIImage(named: element.icon ?? "")
        }
        dayTempLabel.text = "\(Int(object.temp?.day ?? Double()))˚"
        nightTempLabel.text = "\(Int(object.temp?.night ?? Double()))˚"
    }

}
