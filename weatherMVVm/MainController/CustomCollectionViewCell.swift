//
//  CustomCollectionViewCell.swift
//  weatherMVVm
//
//  Created by Николай on 13.09.21.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
// MARK: - Variable
    static let identifier = "CustomCollectionViewCell"
    var viewModel: ViewModel?
    lazy var timeLabel: UILabel = {
        var timeLabel = UILabel()
        timeLabel.font = UIFont(name: "SFNS Display", size: 15)
        return timeLabel
    }()
    lazy var imageWeather: UIImageView = {
        var imageWeather = UIImageView()
        return imageWeather
    }()
    lazy var tempLabel: UILabel = {
        var tempLabel = UILabel()
        tempLabel.font = UIFont(name: "SFNS Display", size: 16)
        return tempLabel
    }()
// MARK: - Lifecycle function
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(timeLabel)
        contentView.addSubview(imageWeather)
        contentView.addSubview(tempLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        timeLabel.snp.makeConstraints { maker in
            maker.centerX.equalToSuperview()
            maker.top.equalToSuperview().inset(12)
        }
        imageWeather.snp.makeConstraints { maker in
            maker.top.equalTo(timeLabel).inset(35)
            maker.leading.trailing.equalToSuperview()
            maker.bottom.equalTo(tempLabel).inset(35)
        }
        tempLabel.snp.makeConstraints { maker in
            maker.centerX.equalToSuperview()
            maker.bottom.equalToSuperview().inset(0)
        }
    }
    
    override func didMoveToSuperview() {
            super.didMoveToSuperview()
            addSubview(SeparatorManager.shared.setSeparatorLineFor(style: 1, width: UIApplication.shared.windows[0].frame.width))
            addSubview(SeparatorManager.shared.setSeparatorLineFor(style: 3, width: UIApplication.shared.windows[0].frame.width))
        }
// MARK: - func config for cell
    func configure(with object: Hourly, indexPath: IndexPath) {
        timeLabel.text = object.dt?.getFormattedDate(format: "HH")
        for image in object.weather {
            imageWeather.image = UIImage(named: image.icon ?? "")
        }
        tempLabel.text = "\(Int(object.temp ?? Double()))˚"
        if indexPath.item == 0 {
            timeLabel.text = "Now"
        }
        if object.dt?.getFormattedDate(format: "HH") == viewModel?.currentModel.value?.sunrise?.getFormattedDate(format: "HH") {
            timeLabel.text = "Sunrise"
            imageWeather.image = UIImage(named: "sunRise")
        }
        if object.dt?.getFormattedDate(format: "HH") == viewModel?.currentModel.value?.sunset?.getFormattedDate(format: "HH") {
            timeLabel.text = "Sunset"
            imageWeather.image = UIImage(named: "sunSet")
        }
    }
}
