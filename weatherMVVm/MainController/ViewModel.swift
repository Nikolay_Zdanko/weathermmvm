//
//  ViewModel.swift
//  weatherMVVm
//
//  Created by Николай on 8.09.21.
//

import Foundation
import Moya

class ViewModel {
// MARK: - Variable
    var moyaProvider = MoyaProvider<WeatherService>()
    var jsonModel = Bindable<Model?>(nil)
    var iconWether = Bindable<String?>(nil)
    var timeZone = Bindable<String?>(nil)
    var currentTemp = Bindable<Double?>(nil)
    var dateNow = Bindable<String>("")
    var mainWeather = Bindable<String?>(nil)
    var hourlyArray = Bindable<[Hourly]?>(nil)
    var maxTemp = Bindable<String?>(nil)
    var minTemp = Bindable<String?>(nil)
    var dayliArray = Bindable<[Daily]?>(nil)
    var descriptionError = Bindable<String?>(nil)
    var currentModel = Bindable<Current?>(nil)
    
    func getRequestModel() {
        LocationManager.shared.getLocation { [weak self] location in
            guard let self = self else { return }
            LocationManager.shared.resolveLocationName(with: location)
                self.moyaProvider.request(.onecall(lat: location.coordinate.latitude, lon: location.coordinate.longitude)) { [weak self] result in
                    guard let self = self else { return }
                    switch result {
                    case .success(let response):
                        do {
                            let model = try JSONDecoder().decode(Model.self, from: response.data)
                            self.jsonModel.value = model
                            self.timeZone.value = model.timezone
                            self.currentTemp.value = model.current.temp
                            for element in model.daily {
                                self.maxTemp.value = "Max \(Int(element.temp?.max ?? Double()))˚"
                                self.minTemp.value = "Min \(Int(element.temp?.min ?? Double()))˚"
                            }
                            for element in model.current.weather {
                                self.mainWeather.value = element.main
                                self.descriptionError.value = element.description
                            }
                            self.hourlyArray.value = model.hourly
                            self.dayliArray.value = model.daily
                            self.currentModel.value = model.current
                        } catch let error {
                            print(error.localizedDescription)
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
        }
    func getDate() {
        let date = Date()
        dateNow.value = date.getFormattedDate(format: "EEEE d MMMM HH:mm") // Set output formate
    }
}
