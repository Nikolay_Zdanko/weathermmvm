//
//  UIAlert + extension.swift
//  weatherMVVm
//
//  Created by Николай on 20.09.21.
//

import Foundation
import UIKit
extension UIAlertController {
    
    static func showNetworkUnavailability(on controller: UIViewController?,
                                          title: String,
                                          message: String,
                                          preferredStyle: UIAlertController.Style) {
        
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: preferredStyle)
        alertController.addAction(UIAlertAction(title: "Settings",
                                                style: .default,
                                                handler: { (_) in
                                                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
                                                                              options: [:],
                                                                              completionHandler: nil)
                                                }))
        alertController.addAction(UIAlertAction(title: "OK",
                                                style: .destructive,
                                                handler: nil))
        
        DispatchQueue.main.async {
            controller?.present(alertController, animated: true, completion: nil)
        }
    }
}
