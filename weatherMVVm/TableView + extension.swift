//
//  TableView + extension.swift
//  weatherMVVm
//
//  Created by Николай on 17.09.21.
//

import Foundation
import UIKit

extension UITableViewCell {
    //MARK: - Flow Functions
    func maskCell(fromTop margin: CGFloat) {
        layer.mask = visibilityMask(withLocation: margin/frame.size.height)
        layer.masksToBounds = true
    }

    private func visibilityMask(withLocation location: CGFloat) -> CAGradientLayer {
        let mask = CAGradientLayer()
        mask.frame = bounds
        mask.colors = [UIColor.white.withAlphaComponent(0).cgColor, UIColor.white.cgColor]
        let num = location as NSNumber
        mask.locations = [num, num]
        return mask
    }
}

extension UIScrollView {
    func setupMaskCellForScrollView(scrollView: UIScrollView, tableView: UITableView) {
        let y: CGFloat = scrollView.contentOffset.y
        for cell in tableView.visibleCells {
            let paddingToDissapear: CGFloat = 0
            let hiddenFrameHeight = y + paddingToDissapear - cell.frame.origin.y
            if hiddenFrameHeight >= 0 || hiddenFrameHeight <= cell.frame.size.height {
                if let customCell = cell as? CustomTableViewCell {
                    customCell.maskCell(fromTop: hiddenFrameHeight)
                }
                if let customCell = cell as? DescriptionTableViewCell {
                    customCell.maskCell(fromTop: hiddenFrameHeight)
                }
                if let customCell = cell as? PropertiesTableViewCell {
                    customCell.maskCell(fromTop: hiddenFrameHeight)
                }
                if let customCell = cell as? OtherTableViewCell {
                    customCell.maskCell(fromTop: hiddenFrameHeight)
                }
            }
        }
    }
}
