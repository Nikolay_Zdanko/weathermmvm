//
//  ModelForecast.swift
//  weatherMVVm
//
//  Created by Николай on 17.09.21.
//

import Foundation
struct ModelForecast: Codable {
    var list: [List]
    var city: City
}
struct List: Codable {
    var dt: Date
    var main: Main
    var weather: [Weather]
}
struct City: Codable {
    var name: String
    var coord: Coord
}
struct Coord: Codable {
    var lat: Double
    var lon: Double
}
struct Main: Codable {
    var temp: Double
}
struct Weather: Codable {
    var main: String
}
