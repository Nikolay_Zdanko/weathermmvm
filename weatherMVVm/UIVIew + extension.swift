//
//  UIVIew + extension.swift
//  weatherMVVm
//
//  Created by Николай on 20.09.21.
//

import Foundation
import UIKit

extension UIView {

    func clearWeatherGradient(view: UIView, color: [CGColor]) {
        let layer = CAGradientLayer()
        layer.frame = self.bounds
        layer.colors = color
        
        layer.locations = [0, 1]
        layer.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer.endPoint = CGPoint(x: 0.75, y: 0.5)
        
        layer.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 0, b: 1, c: -1, d: 0, tx: 1, ty: 0))
        
        layer.bounds = view.bounds.insetBy(dx: -1 * view.bounds.size.width, dy: -1 * view.bounds.size.height)
        layer.position = view.center
        self.layer.insertSublayer(layer, at: 0)
    }
    
    func setupGradient(main: String) {
        switch main {
        case "Clear":
            self.clearWeatherGradient(view: self, color: [
                UIColor(red: 0.934, green: 0.947, blue: 0.958, alpha: 1).cgColor,
                UIColor(red: 0.762, green: 0.825, blue: 0.883, alpha: 1).cgColor])
        case "Clouds" :
            self.clearWeatherGradient(view: self, color:[
                UIColor(red: 0.971, green: 0.986, blue: 1, alpha: 1).cgColor,
                UIColor(red: 0.679, green: 0.846, blue: 1, alpha: 1).cgColor])
        case "FewClouds":
            self.clearWeatherGradient(view: self, color:[
                UIColor(red: 0.917, green: 0.96, blue: 1, alpha: 1).cgColor,
                UIColor(red: 0.854, green: 0.832, blue: 0.79, alpha: 1).cgColor])
        case "Mainly":
            self.clearWeatherGradient(view: self, color:[
                UIColor(red: 0.917, green: 0.96, blue: 1, alpha: 1).cgColor,
                UIColor(red: 0.854, green: 0.832, blue: 0.79, alpha: 1).cgColor])
        case "Mist":
            self.clearWeatherGradient(view: self, color:[
                UIColor(red: 0.773, green: 0.822, blue: 0.867, alpha: 1).cgColor,
                UIColor(red: 0.921, green: 0.921, blue: 0.921, alpha: 1).cgColor])
        case "Rain":
            self.clearWeatherGradient(view: self, color:[
                UIColor(red: 0.934, green: 0.947, blue: 0.958, alpha: 1).cgColor,
                UIColor(red: 0.762, green: 0.825, blue: 0.883, alpha: 1).cgColor])
        case "Snow":
            self.clearWeatherGradient(view: self, color:[
                UIColor(red: 0.788, green: 0.87, blue: 0.946, alpha: 1).cgColor,
                UIColor(red: 0.71, green: 0.759, blue: 0.804, alpha: 1).cgColor])
        case "ThunderStorm":
            self.clearWeatherGradient(view: self, color:[
                UIColor(red: 0.768, green: 0.765, blue: 0.892, alpha: 1).cgColor,
                UIColor(red: 0.938, green: 0.97, blue: 1, alpha: 1).cgColor])
        default:
            break
        }
    }
}
