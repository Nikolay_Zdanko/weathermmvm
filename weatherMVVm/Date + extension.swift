//
//  Date + extension.swift
//  weatherMVVm
//
//  Created by Николай on 17.09.21.
//

import Foundation
import UIKit

extension Int {
 func addDailyDate(date: Int, dateFormat: String) -> String {
    let date = Date(timeIntervalSince1970: TimeInterval(date))
    let formatter = DateFormatter()
    formatter.dateFormat = dateFormat
    let hourDate = formatter.string(from: date).capitalized
    return hourDate
 }
}

extension Date {
   func getFormattedDate(format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: self)
    }
}
extension Date {
    func getFormattedDate(format: String, value: Int?) -> String {
        let date = Calendar.current.date(byAdding: .day, value: value ?? Int(), to: Date())
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date ?? Date())
    }
}
