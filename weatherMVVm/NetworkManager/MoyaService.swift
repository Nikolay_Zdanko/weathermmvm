//
//  MoyaService.swift
//  weatherMVVm
//
//  Created by Николай on 18.09.21.
//

import Foundation
import Moya
import CoreLocation

enum WeatherService {
    case onecall(lat: Double, lon: Double)
    case forecast(lat: Double, lon: Double)
}

extension WeatherService: TargetType {
        
    var baseURL: URL {
        return URL(string: "https://api.openweathermap.org/data/2.5")!
    }
    
    var path: String {
        switch self {
        case .onecall:
            return "/onecall"
        case .forecast:
            return "/forecast"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .onecall:
            return .get
        case .forecast:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case let .onecall(lat,lon):
            return .requestParameters(parameters: ["lat":"\(lat)",
                                                   "lon": "\(lon)",
                                                   "units": "metric",
                                                   "appid":"22bde89beaa3c94eb6848745aa74d729"],encoding: URLEncoding.default)
        case let .forecast(lat,lon):
            return .requestParameters(parameters: ["lat":"\(lat)",
                                                   "lon": "\(lon)",
                                                   "units": "metric",
                                                   "appid":"22bde89beaa3c94eb6848745aa74d729"], encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
}
