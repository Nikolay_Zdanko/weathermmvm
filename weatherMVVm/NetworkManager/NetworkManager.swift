//
//  NetworkManager.swift
//  weatherMVVm
//
//  Created by Николай on 10.09.21.
//

import Foundation
class NetworkManager {
func getRequest(url: String, comletion: @escaping (Model) -> Void) {
        sendRequest(urlString: url) { (data) in
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
               let json = try decoder.decode(Model.self, from: data)
                DispatchQueue.main.async {
                    comletion(json)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }

    func sendRequest( urlString: String, completion: @escaping (Data) -> Void) {
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            if error == nil, let data = data {
            completion(data)
            } else {
                // нету инета ошибка
                print(error?.localizedDescription)
            }
        }.resume()
    }

func getRequestForecast(url: String, comletion: @escaping (ModelForecast) -> Void) {
        sendRequest(urlString: url) { (data) in
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
               let json = try decoder.decode(ModelForecast.self, from: data)
                DispatchQueue.main.async {
                    comletion(json)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }

    func sendRequestForecast( urlString: String, completion: @escaping (Data) -> Void) {
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            if error == nil, let data = data {
            completion(data)
            } else {
                // нету инета ошибка
                print(error?.localizedDescription ?? "")
            }
        }.resume()
    }

}
