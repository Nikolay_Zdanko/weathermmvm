//
//  SeparatorManager.swift
//  weatherMVVm
//
//  Created by Николай on 13.09.21.
//

import UIKit
import Foundation

class SeparatorManager {
    let indent: CGFloat = 20
    static let shared = SeparatorManager()
    private init () {}

    
    func setSeparatorLineFor(style: Int, width: CGFloat) -> UIView {
        let line = UIView()
        line.backgroundColor = .white
        switch style {
        case 1:
            line.frame = CGRect(x: 0, y: 5, width: width, height: 2)
            return line
        case 2:
            line.frame = CGRect(x: indent, y: 0, width: width - 2 * indent, height: 1)
            return line
        case 3:
            line.frame = CGRect(x: 0, y: 124, width: width, height: 2)
            return line
        case 4:
            line.frame = CGRect(x: 0, y: 45, width: width, height: 2)
            return line
        default:
            return UIView()
        }
    }
    
}
